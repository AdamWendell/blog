module Main exposing (..)

import Navigation exposing (program)
import Html exposing (Html, text, div, img, a)
import Route exposing (..)


type alias Model =
    { page : Route }


init : Navigation.Location -> ( Model, Cmd msg )
init location =
    ( { page = route (Route.fromLocation location) }, Cmd.none )


type Msg
    = Change Route


navigation : Route -> Model -> ( Model, Cmd Msg )
navigation route model =
    ( { model | page = route }, Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Change route ->
            navigation route model


view : Model -> Html Msg
view model =
    div []
        [ a [ Route.href Home ] [ text "Home" ]
        , a [ Route.href About ] [ text "About" ]
        ]


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


route : Maybe Route -> Route
route maybeRoute =
    Maybe.withDefault Notfound maybeRoute


main : Program Never Model Msg
main =
    program (Route.fromLocation >> route >> Change) { view = view, init = init, update = update, subscriptions = subscriptions }
